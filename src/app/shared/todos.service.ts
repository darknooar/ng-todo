import { Injectable } from '@angular/core';
import { Todo } from './todo.model';

@Injectable()
export class TodosService {

    data: Todo[] = [
        { id: 0, name: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates quod accusantium earum! Ratione, enim a optio magni ipsa id repudiandae unde fugit exercitationem nemo officia, officiis minima. Eius, adipisci sunt explicabo nesciunt. Aliquid, natus, cupiditate? Alias, libero, a. Repellendus magnam corporis asperiores, necessitatibus vitae quod provident ab eum placeat reprehenderit velit illo vel perspiciatis consectetur autem fugiat nobis sequi. Dolorem porro incidunt vitae sapiente quo libero qui voluptatum ipsam nisi perferendis deserunt distinctio iste voluptatem delectus, sit? Hic molestiae placeat quaerat, esse, magnam dignissimos commodi repellendus, quis veniam voluptate quibusdam facere quidem illo tempora autem accusantium. Quasi perferendis blanditiis voluptatem.', completed: false },
        { id: 1, name: 'Do__2', completed: false },
        { id: 2, name: 'Do__3', completed: false },
        { id: 3, name: 'Do__4', completed: false },
        { id: 4, name: 'Do__5', completed: false },
        { id: 5, name: 'Do__6', completed: true  }
    ];

    getAll(): Todo[]{
        return this.data;
    }

    getById(id: number): Todo{
        return this.data.find(x => x.id == id);
    }

    update (todo: Todo){
        let toUpdate = this.getById(todo.id);
        Object.assign(toUpdate, todo);
    }

    create (todo: Todo){
        this.data.push(todo);
    }

    delete (todo: Todo){
        let toDelete = this.getById(todo.id);
        this.data.splice(this.data.indexOf(toDelete), 1);
    }

}