import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { SettingsComponent } from './settings/settings.component';
import { TodosService } from './shared/todos.service';
import { TodoEditorComponent } from './todo-editor/todo-editor.component';
import { TodoCreatorComponent } from './todo-creator/todo-creator.component';


@NgModule({
	declarations: [
		AppComponent,
		TodoListComponent,
		SettingsComponent,
		TodoEditorComponent,
		TodoCreatorComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule
	],
	providers: [TodosService],
	bootstrap: [AppComponent]
})
export class AppModule { }
