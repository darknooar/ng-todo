import { Component, OnInit } from '@angular/core';
import { TodosService } from '../shared/todos.service';
import { Todo } from '../shared/todo.model';
import { Router } from '@angular/router';

@Component({
	selector: 'app-todo-list',
	templateUrl: './todo-list.component.html',
	styleUrls: ['./todo-list.component.styl']
})
export class TodoListComponent implements OnInit {
	
	todos: Todo[];

	constructor(
			private service: TodosService,
			private router: Router,
		){ 
		
	}

	ngOnInit() {
		this.todos = this.service.getAll();
	}

	onCreate(){
		this.router.navigate(['list', 'create'])
	}

	onEdit(todo: Todo){
		// console.log(name + ' edited');
		this.router.navigate(['list', 'edit', todo.id])
	}

	onDelete(todo:number){
		let toDelete = this.service.getById(todo);
		this.service.data.splice(this.service.data.indexOf(toDelete), 1);

		// this.service.delete(todo)
		
	}

}
