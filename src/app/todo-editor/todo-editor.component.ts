import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TodosService } from '../shared/todos.service';
import { Todo } from '../shared/todo.model';

@Component({
  selector: 'app-todo-editor',
  templateUrl: './todo-editor.component.html',
  styleUrls: ['./todo-editor.component.styl']
})
export class TodoEditorComponent implements OnInit {
  todo: Todo;

  constructor(
    private currentRouter: ActivatedRoute,
    private service: TodosService,
  ) { }

  ngOnInit() {
    let id: number = +this.currentRouter.snapshot.paramMap.get('id');
    this.todo = this.service.getById(id);
  }

}
