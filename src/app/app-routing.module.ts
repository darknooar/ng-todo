import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoListComponent } from './todo-list/todo-list.component';
import { SettingsComponent } from './settings/settings.component';
import { TodoEditorComponent } from './todo-editor/todo-editor.component';

const routes: Routes = [
  { path: 'list', component: TodoListComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'list/edit/:id', component: TodoEditorComponent },
  { path: '**', redirectTo: 'list', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
